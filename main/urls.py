from django.urls import path
from main.views import home, soon, profile, experiences, skills

app_name = "Story_4"

urlpatterns = [
    path('', home, name="home"),
    path('soon', soon, name="soon"),
    path('profile', profile, name="profile"),
    path('experiences', experiences, name="experiences"),
    path('skills', skills, name="skills")
]